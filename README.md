# Dispositivo virtual

## Criando o dispositivo

**É necessário ter o NodeJs instalado**

### Inicializando o projeto

Primeiro crie uma pasta separada para conter o projeto, use o terminal para dar cd até chegar nesse repositório e digite `npm init` e preencha o que ele pedir, caso queira preencher tudo com o padrão utilize o `npm init -y` , isso ira criar um arquivo chamado package.json com todas as informações do projeto, após isso digite `npm i saiot-device` para instalar a biblioteca do saiot.

```sh
# Crie uma pasta separada para conter o projeto
$ mkdir meuProjeto
# Mude o diretório para a pasta criada
$ cd meuProjeto
# Inicialize o projeto e preencha os dados solicitados
$ npm init
# Instale a biblioteca do Saiot
$ npm i saiot-device
```

### Criando o arquivo principal

Crie um arquivo terminando com .js (preferencialmente é bom deixar o nome do arquivo como index.js), é bom o nome desse arquivo ser o mesmo do campo main do package.json.

Além disso, nós recomendamos o uso do editor de texto [Visual Studio Code](https://code.visualstudio.com/).

### Código base

O código abaixo é um exemplo de um dispositivo básico

```jsx
const Saiot = require("saiot-device");

const saiotDevice = new Saiot({
  email: "seu email aqui",
  password: "sua senha aqui",
  name: "Lampada sala",
  class: "lampada",
  description: "Uma lampada fluorescente",
  sensors: {
    onOff: {
      name: "Sensor booleano",
      type: "on/off",
      timeout: 1,
      value: false,
    },
    number: {
      name: "Sensor numérico",
      type: "number",
      timeout: 1,
      value: 0,
    },
  },
  actuators: {
    switch: {
      name: "Interruptor",
      type: "switch",
      value: false,
    },
    number: {
      name: "Numeral",
      type: "number",
      value: 0,
    },
  },
});

// Atuadores
let switchActuatorState = false;
let numberActuatorState = 0;

function switchActuator(value) {
  console.log(
    `Mudando o estado da lampada de ${switchActuatorState} para ${value}`
  );
  switchActuatorState = value;
  return switchActuatorState;
}

function numberActuator(value) {
  console.log(
    `Mudando o estado da lampada de ${numberActuatorState} para ${value}`
  );
  numberActuatorState = value;
  return numberActuatorState;
}

// Sensores
let onOffSensorState = false;
let numberSensorState = 0;

function getonOffSensorState() {
  onOffSensorState = !onOffSensorState;
  return onOffSensorState;
}

function getNumberSensorState() {
  numberSensorState = Number((28 + (Math.random() * 3 - 1)).toPrecision(4));
  return numberSensorState;
}

function setup() {
  saiotDevice.start();
  saiotDevice.setActuatorCallback("switch", switchActuator);
  saiotDevice.setActuatorCallback("number", numberActuator);
  saiotDevice.setTimeoutCallback("onOff", getonOffSensorState);
  saiotDevice.setTimeoutCallback("number", getNumberSensorState);
}

setup();
```

### Exemplos

Mais exemplos de dispositivos podem ser encontrados na pasta [examples](https://gitlab.com/saiot/saiot-2.0/simulated-device/examples) do repositório. São eles:

- all - Dispositivo com vários tipos de sensores e atuadores
- lampada - Lâmpada fluorescente
- lampada-rgb - Lâmpada RGB
- monitor-de-sistema - Monitor de componentes de sistema (CPU e RAM)
- gps - GPS

> Nota: Ao utilizar exemplos em outros projetos, é necessário instalar as bibliotecas necessárias para cada dispositivo. Além disso, trocar a importação da biblioteca de `const Saiot = require("../../index");` para `const Saiot = require("saiot-device");`

# Saiot

## Construtor

```jsx
new Saiot({
  brokerUrl: "dev", // Opcional | URL de conexão com o broker
  email: "seu email aqui", // Obrigatório | email do seu usuário saiot
  password: "sua senha aqui", // Obrigatório | senha do seu usuário saiot
  name: "Lampada sala", // Obrigatório | nome do dispositivo
  class: "lampada", // Obrigatório, atualmente não tem motivo para ser usada
  description: "Uma lampada fluorescente", // Opcional | descrição do dispositivo
  sensors: {
    // Objeto contendo todos os sensores
    light: {
      // O nome do campo do objeto é o id do sensor
      name: "Luz", // Obrigatório | nome do sensor
      description: "Luz fluorescente", // Opcional | nome do sensor
      type: "on/off", // Obrigatório | tipo do sensor
      timeout: 1, // Opcional | tempo do intervalo de envio em segundos
      value: false, // Obrigatório | valor inicial
    },
  },
  actuators: {
    // Objeto contendo todos os atuadores
    number: {
      // O nome do campo do objeto é o id do atuador
      name: "Numeral", // Obrigatório | nome do sensor
      type: "number", // Obrigatório | tipo do atuador
      value: 0, // Obrigatório | valor inicial
    },
  },
});
```

> O campo `type` é apenas utilizado no front pra renderizar um card apropriado, em teoria o campo `value` pode assumir qualquer valor (objeto, numero, array, etc) mas coloque o valor apropriado condizente com o `type` para não gerar erros na renderização no site

> Um campo não utilizado foi o `deadband` que fica dentro do sensor, esse campo é utilizado em conjunto com o `timeout` e serve para dizer para só enviar o dado caso seja diferente x% do valor anterior sendo o x o valor do `deadband`

> O campo `brokerUrl` aceita uma URL MQTT no formato "mqtt://dominio:porta" ou a string "dev", que conecta-se ao broker versão desenvolvimento do Saiot

## Métodos

### start()

Esse comando é utilizado para iniciar a comunicação do dispositivo com a plataforma Saiot

### getJsonConfig()

Retorna o json de configuração do dispositivo

### sendSensorData(id, value)

Envia de maneira direta o valor atualizado do sensor

**Parametros**

- id: string
- value: any

**Exemplo**

```jsx
saiotDevice.sendSensorData("light", true);
```

### reportSensorData()

Semelhante ao sendSensorData, porém, ele envia utlizando o critério do deadband

**Parametros**

- id: string
- value: any

**Exemplo**

```jsx
// Se 28.5 for diferente de x% do ultimo v
saiotDevice.reportSensorData("termometro", 28.5);
```

### setTimeoutCallback(id, callback, options?)

Utilizada para indicar qual o valor a ser reportado no intervalo do timeout

**Parametros**

- id: string
- callback(): any
- [opcional] options: object(maxValue: number)
  - maxValue: Atribui o valor máximo do sensor, a ser exibido na interface

**Exemplo**

```jsx
function getSensorValue() {
  return Math.random() >= 0.5;
}

saiotDevice.setTimeoutCallback("light", getSensorValue);
```

### setActuatorCallback(id, callback)

O callback precisa ser uma função que vai ser chamada quando o atuador for atualizado pelo front

**Parametros**

- id: string
- callback(): any

**Exemplo**

```jsx
function numberActuator(value) {
  console.log(`Recebido valor ${value}`);
  return value;
}

saiotDevice.setActuatorCallback("number", numberActuator);
```
