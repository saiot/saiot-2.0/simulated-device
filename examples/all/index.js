const Saiot = require("../../index");

const saiotDevice = new Saiot({
  email: "teste@teste.com",
  password: "0123456789",
  name: "Dispositivo geral",
  class: "geral",
  description: "Um dispositivo com todos os atuadores/sensores disponiveis",
  sensors: {
    onOff: {
      name: "Sensor booleano",
      type: "on/off",
      timeout: 1,
      value: false,
    },
    number: {
      name: "Sensor numérico",
      type: "number",
      timeout: 1,
      value: 0,
    },
  },
  actuators: {
    switch: {
      name: "Interruptor",
      type: "switch",
      value: false,
    },
    number: {
      name: "Numeral",
      type: "number",
      value: 0,
    },
  },
});

// Atuadores
let switchActuatorState = false;
let numberActuatorState = 0;

function switchActuator(value) {
  console.log(
    `Mudando o estado da lampada de ${switchActuatorState} para ${value}`
  );
  switchActuatorState = value;
  return switchActuatorState;
}

function numberActuator(value) {
  console.log(
    `Mudando o estado da lampada de ${numberActuatorState} para ${value}`
  );
  numberActuatorState = value;
  return numberActuatorState;
}

// Sensores
let onOffSensorState = false;
let numberSensorState = 0;

function getonOffSensorState() {
  onOffSensorState = !onOffSensorState;
  return onOffSensorState;
}

function getNumberSensorState() {
  numberSensorState = Number((28 + (Math.random() * 3 - 1)).toPrecision(4));
  return numberSensorState;
}

function setup() {
  saiotDevice.start();
  saiotDevice.setActuatorCallback("switch", switchActuator);
  saiotDevice.setActuatorCallback("number", numberActuator);
  saiotDevice.setTimeoutCallback("onOff", getonOffSensorState);
  saiotDevice.setTimeoutCallback("number", getNumberSensorState);
}

setup();
