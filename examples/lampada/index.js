const Saiot = require("../../index");

const saiotDevice = new Saiot({
  email: "teste@teste.com",
  password: "0123456789",
  name: "Lampada sala",
  class: "lampada",
  description: "Uma lampada fluorescente",
  actuators: {
    switch: {
      name: "Interruptor",
      type: "switch",
      value: false,
    },
  },
});

let lampState = false;

function lightSwitch(value) {
  console.log(`Mudando o estado da lampada de ${lampState} para ${value}`);
  lampState = value;
  return lampState;
}

function setup() {
  saiotDevice.start();
  saiotDevice.setActuatorCallback("switch", lightSwitch);
}

function loop() {}

setup();
setInterval(loop, 5000);
