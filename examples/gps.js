const Saiot = require("../index");

const saiotDevice = new Saiot({
  email: "teste@teste.com",
  password: "0123456789",
  name: "Rastreador GPS",
  class: "rastreador",
  description: "Um simples rastreador GPS",
  sensors: {
    gps: {
      name: "GPS",
      type: "gps",
      timeout: 1,
      value: [-5.842552, -35.197645],
    },
  },
});

let globalPosition = {
  latitude: -5.842552,
  longitude: -35.197645,
};

function getGlobalPosition() {
  globalPosition.latitude += (Math.random() * 2 - 1) / 10000;
  globalPosition.longitude += (Math.random() * 2 - 1) / 10000;

  console.log(
    `Movendo para: lat=${globalPosition.latitude}, long=${globalPosition.longitude}`
  );
  return [globalPosition.latitude, globalPosition.longitude];
}

function setup() {
  saiotDevice.start();
  saiotDevice.setTimeoutCallback("gps", getGlobalPosition);
}

setup();
