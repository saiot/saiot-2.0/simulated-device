const Saiot = require("../../index");

const saiotDevice = new Saiot({
  email: "teste@teste.com",
  password: "0123456789",
  name: "Lampada RGB",
  class: "lampada-rgb",
  description: "Uma lampada rgb",
  actuators: {
    led: {
      name: "Luz",
      type: "color",
      value: [0, 0, 0, 0],
    },
    strobo: {
      name: "Strobo",
      type: "slider",
      min: 0,
      max: 40,
      step: 1,
      value: 1,
    },
  },
});

let lampState = [0, 0, 0, 0];
let stroboState = 1;

function ledCallback(r, g, b, a) {
  const value = [r, g, b, a];
  console.log(`Mudando o estado da lampada de ${lampState} para ${value}`);
  lampState = value;
  return lampState;
}

function stroboCallback(value) {
  console.log(`Mudando o estado do strobo de ${stroboState} para ${value}`);
  stroboState = value;
  return stroboState;
}

function setup() {
  saiotDevice.start();
  saiotDevice.setActuatorCallback("led", ledCallback);
  saiotDevice.setActuatorCallback("strobo", stroboCallback);
}

function loop() {}

setup();
setInterval(loop, 5000);
