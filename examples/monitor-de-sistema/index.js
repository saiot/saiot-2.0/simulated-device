require("loadavg-windows"); // Necessário para sistemas operacionais Windows
const os = require("os");
const Saiot = require("../../index");

const saiotDevice = new Saiot({
  email: "teste@teste.com",
  password: "0123456789",
  name: "Monitor de sistema",
  class: "monitor-de-sistema",
  description:
    "Monitora dados de uso de componentes do sistema, como CPU, RAM, etc",
  sensors: {
    cpu: {
      name: "Uso de CPU",
      type: "number",
      timeout: 1,
      value: 0,
      unit: "%",
    },
    ram: {
      name: "Uso de RAM",
      type: "number",
      timeout: 1,
      value: 0,
      unit: "GB",
    },
  },
});

function convertFromBytesToGB(numberInBytes) {
  return numberInBytes / (1024 * 1024 * 1024);
}

function getCpuUsage() {
  const cpuLoad = os.loadavg();
  const numberOfCores = Object.keys(os.cpus()).length;
  const usedCPUInPercentage = (cpuLoad[0] * 100) / numberOfCores;

  return usedCPUInPercentage.toFixed(2);
}

function getRamUsage() {
  const totalMemory = os.totalmem();
  const freeMemory = os.freemem();
  const usedMemoryInGB = convertFromBytesToGB(totalMemory - freeMemory);

  return usedMemoryInGB.toFixed(2);
}

function getRamTotal() {
  const totalMemoryInGB = convertFromBytesToGB(os.totalmem());

  return totalMemoryInGB.toFixed(2);
}

function setup() {
  saiotDevice.start();

  saiotDevice.setTimeoutCallback("cpu", getCpuUsage);
  saiotDevice.setTimeoutCallback("ram", getRamUsage, {
    maxValue: getRamTotal(),
  });
}

function loop() {}

setup();
loop();
