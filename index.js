const EventEmitter = require("events");
const mqtt = require("mqtt");
const fs = require("fs");
const path = require("path");

module.exports = class Saiot extends EventEmitter {
  constructor(config) {
    super();
    this.brokerUrl = config.brokerUrl;
    this.email = config.email;
    this.password = config.password;
    this.name = config.name;
    this.class = config.class;
    this.description = config.description;
    this.sensors = config.sensors;
    this.actuators = config.actuators;
    this.actuatorsCallback = {};
    this.timeoutIntervals = {};
    this.timeoutCallback = {};
    this.lastReportedValue = {};
    this.isLogged = false;
  }

  _searchId() {
    try {
      // Isso é uma forma de simular um eeprom
      this.id = fs.readFileSync(path.resolve(__dirname, "id")).toString();
      console.log("> Id encontrando");
    } catch (err) {
      console.log("> Id não encontrado");
    }
  }

  _connectMqtt() {
    const brokerUrl = !this.brokerUrl
      ? "mqtt://api.saiot2.ect.ufrn.br:8000"
      : this.brokerUrl === "dev"
      ? "mqtt://dev.api.saiot2.ect.ufrn.br:8000"
      : this.brokerUrl;

    this.client = mqtt.connect(brokerUrl, {
      username: this.email,
      password: this.password,
      clientId: this.id,
      reconnectPeriod: 0,
    });

    this.client.on("connect", () => {
      const { clientId } = this.client.options;

      this.client.subscribe(`${clientId}/message`, null, () => {
        console.log(`> Inscrito no tópico ${clientId}/message`);
      });
      this.client.subscribe(`${clientId}/config`, null, () => {
        console.log(`> Inscrito no tópico ${clientId}/config`);
      });
      this.client.subscribe(`${clientId}/act`, null, () => {
        console.log(`> Inscrito no tópico ${clientId}/act`);
      });
    });

    this.client.on("message", (topic, message) => {
      this._callback(topic, message);
    });

    this.client.on("close", () => {
      this.emit("disconnected");
    });

    this.client.on("error", (error) => console.log(`> Erro: ${error.message}`));
  }

  start() {
    this._searchId();
    this._connectMqtt();
  }

  getJsonConfig() {
    return JSON.stringify({
      id: this.id,
      email: this.email,
      name: this.name,
      class: this.class,
      description: this.description,
      sensors: this.sensors,
      actuators: this.actuators,
    });
  }

  _updateConfig(newConfig) {
    Object.keys(this.timeoutIntervals).forEach((sensorId) => {
      if (
        newConfig.sensors[sensorId].timeout !== this.sensors[sensorId].timeout
      ) {
        clearInterval(this.timeoutIntervals[sensorId]);

        this.timeoutIntervals[sensorId] = setInterval(() => {
          this.reportSensorData(sensorId, this.timeoutCallback[sensorId]());
        }, newConfig.sensors[sensorId].timeout * 1000);
      }
    });

    this.name = newConfig.name;
    this.class = newConfig.class;
    this.description = newConfig.description;
    this.sensors = newConfig.sensors;
    this.actuators = newConfig.actuators;
  }

  _callback(topic, message) {
    if (this.id) {
      const { clientId } = this.client.options;
      switch (topic) {
        case clientId + "/message":
          switch (message.toString()) {
            case "Logado":
              console.log("> Logado no broker com sucesso");
              this.isLogged = true;
              this.emit("connected");
              break;
            case "Cadastre-se":
              this.client.publish("register-device", this.getJsonConfig());
              console.log("> Enviando solicitação de cadastro");
              break;
            case "Aprovado":
              this.isLogged = true;
              this.emit("connected");
              this.client.end(true, null, () => {
                this.client.reconnect();
              });
              console.log("> Cadastro aprovado");
              break;
            case "Recusado":
              console.log("> Cadastro recusado, encerrando...");
              process.exit();
            case "Deletado":
              console.log("> Dispositivo deletado pelo usuário, encerrando...");
              process.exit();
            case "Aguardando aprovação":
              console.log("> Aguardando aprovação do usuário");
              break;
            default:
              console.log(
                "> Nova mensagem recebida pela api:",
                message.toString()
              );
              break;
          }
          break;
        case clientId + "/config":
          console.log("> Atualizando jsonConfig");
          this._updateConfig({
            id: clientId,
            email: this.email,
            ...JSON.parse(message),
          });
          break;
        case clientId + "/act":
          const { id, parameters } = JSON.parse(message);

          const value = this.actuatorsCallback[id](...parameters);

          this.client.publish(
            `act-return/${clientId}`,
            JSON.stringify({ id, value })
          );

          console.log(
            `> Ação recebida para o controlador '${id}' os parametros '${parameters}'`
          );
          break;
        default:
          console.log(
            `> Nova mensagem\nTopico: ${topic}\nMensagem: ${message.toString()}`
          );
          break;
      }
    } else {
      this.id = message.toString();
      fs.writeFileSync(path.resolve(__dirname, "id"), this.id);
      console.log("Novo id recebido");
      this._connectMqtt();
    }
  }

  // Manda a informação direto independente da configuração
  sendSensorData(id, value) {
    if (
      this.isLogged &&
      this.sensors[id] &&
      this.lastReportedValue[id] != value
    ) {
      const { clientId } = this.client.options;

      this.client.publish(
        `measurements/${clientId}`,
        JSON.stringify({ id, value })
      );

      this.lastReportedValue[id] = value;

      console.log(
        `Enviando informação do sensor com id '${id}' o valor '${value}'`
      );
    }
  }

  setTimeoutCallback(id, callback, options = {}) {
    if (!!options.maxValue) {
      this.sensors[id].max = options.maxValue;
    }

    if (this.sensors[id] && this.sensors[id].timeout) {
      if (this.timeoutIntervals[id]) {
        clearInterval(this.timeoutIntervals[id]);
      }

      this.timeoutIntervals[id] = setInterval(() => {
        if (this.isLogged) {
          this.reportSensorData(id, callback());
        }
      }, this.sensors[id].timeout * 1000);

      this.timeoutCallback[id] = callback;
    }
  }

  setActuatorCallback(id, callback) {
    if (this.actuators[id]) {
      this.actuatorsCallback[id] = callback;
    }
  }

  // Faz o envio se baseando na configuração
  reportSensorData(id, value) {
    if (
      this.isLogged &&
      this.sensors[id] &&
      this.lastReportedValue[id] != value
    ) {
      const { deadband } = this.sensors[id];

      if (deadband) {
        if (!isNaN(value) && !isNaN(this.lastReportedValue[id])) {
          if (
            Math.abs(value - this.lastReportedValue[id]) >=
            this.lastReportedValue[id] * (deadband / 100)
          ) {
            this.sendSensorData(id, value);
          }
        } else {
          this.sendSensorData(id, value);
        }
      } else {
        this.sendSensorData(id, value);
      }
    }
  }
};
